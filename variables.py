
############## Data types ##############

num1 = 2   # number
num2 = num1 # number
var3 = 'd' # character
var4 = "shubham" # string
var5 = True

print(var5)

# LIST
fruit = ["banana", "mango", "apple", "banana", "apple", "berry"]
print(fruit)
print("Azhar need to purchase these:", fruit)

# Tuple
flavours = ("banana", "mango", "apple", "banana", "apple", "berry", "biryani")
print(flavours)

# SET
flowers = {"rose", "lily", "jasmine", "lotus", "rose", "lily"}
print(flowers)

# Dictionary
address = {"city": "pune", "state": "mh", "pincode": 411004 }
print("my address is:", address)


## Example of List
fruit = ["banana", "mango", "apple", "banana", "apple", "berry"]
print(fruit)
fruit[2]="dragonfruit"
print("my fav fruit is",fruit[2])
print("my fav fruit is",fruit[2],"and",fruit[3])
print(fruit)


## Example of tuple
# flavours = ("banana", "mango", "apple", "banana", "apple", "berry", "biryani")
# flavours[2] = "dragonfruit"
# print(flavours) ## Touple is immutable. This will generate error.

# Example of map
address = {"city": "pune", "state": "mh", "pincode": 411004 }
print("I live in",address["city"])
# print("I live in",address[0])

# <VAR> = <Value>

